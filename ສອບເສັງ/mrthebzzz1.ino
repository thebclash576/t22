// define pins
#define BLUE 3
#define GREEN 5
#define RED 6
#define button 2

// define color mode
int mode = 0;

void setup() {
  // setup LED lights
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  //setup buttons
  pinMode(button, INPUT_PULLUP);


}
void loop() {

  if (digitalRead(button) == LOW) {
    mode = mode + 1;
    delay(400);
  }
// off
if (mode == 0) {
  analogWrite(BLUE, 0);
  analogWrite(GREEN, 0);
  analogWrite(RED, 0);
}
// Red
if (mode == 1) {
  analogWrite(BLUE, 0);
  analogWrite(GREEN, 0);
  analogWrite(RED, 225);
}
// Blue
if (mode == 2) {
  analogWrite(BLUE, 0);
  analogWrite(GREEN, 255);
  analogWrite(RED, 0);
}
// Green 
if (mode == 3) {
  analogWrite(BLUE, 255);
  analogWrite(GREEN, 0);
  analogWrite(RED, 0);
}

// White
if (mode == 4) {
  analogWrite(BLUE, 225);
  analogWrite(GREEN, 225);
  analogWrite(RED, 225);
}
  
// Switch off
if (mode == 5) {
  mode = 0;
  } 
}